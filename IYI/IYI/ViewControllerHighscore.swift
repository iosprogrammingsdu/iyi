//
//  ViewControllerHighscore.swift
//  IYI
//
//  Created by Daniel Jørgensen & Mikkel Schmøde on 20/11/2018.
//  Copyright © 2018 MD. All rights reserved.
//

import UIKit
import AVFoundation

class ViewControllerHighscore: UIViewController {
    
    @IBOutlet weak var HighScoreLabel: UILabel!
    
    let back = Bundle.main.path(forResource: "back.mp3", ofType:nil)! // back sound
    let correctSound = Bundle.main.path(forResource: "correctSound.wav", ofType:nil)! // back sound
    var audioPlayer = AVAudioPlayer() // the sound player that plays the sounds
    
    // This function loads the highscore when the view has been loaded.
    override func viewDidLoad() {
        loadHighScore()
    }
    
    // This function resets the highscore when the button is pressed.
    @IBAction func ResetHighScoreButton(_ sender: Any) {
        ResetHighScoreData()
        loadHighScore()
    }
    
    // Function of the back button that goes to the last View Controller (This will always be the menu)
    @IBAction func back(_ sender: Any) {
        // Loads the sound file
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: back))
            audioPlayer.play() // plays the sound file
        } catch {
            print("couldn't load sound file")
        }
        dismiss(animated: true, completion: nil)
    }
    
    // function that loads the highscore
    func loadHighScore(){
        let fileName = "HighScore" // The name of the file with the current score
        var readString = "" // initialize varaible where the data will be stored
        let DocumentDirURL = try!  FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true) // The url of the file
        let fileURL = DocumentDirURL.appendingPathComponent(fileName).appendingPathExtension("txt") // Finds the find at the url
        
        do {
            readString = try String(contentsOf: fileURL) // reads the file and saves it in the readString variable
        } catch let error as NSError {
            print(error)
        }
        
        HighScoreLabel.text = readString // sets the label from the data in the string.
    }
    
    //function that resets the highscore
    func ResetHighScoreData(){
        // Local variables
        let fileName = "HighScore" // The name of the file with the current highscore
        let DocumentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true) // The url of the file
        let fileURL = DocumentDirURL.appendingPathComponent(fileName).appendingPathExtension("txt") // The url of the file
        
        let writeScore = String(0) // Converts the writeScore value to a String that contains a 0.
        
        do {
            // writes to file
            try writeScore.write(to: fileURL, atomically: true, encoding: String.Encoding.utf8)
        }
        catch let error as NSError{
            print(error)
        }
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: correctSound))
            audioPlayer.play() // plays the sound file
        } catch {
            print("couldn't load sound file")
        }
    }
    
}
