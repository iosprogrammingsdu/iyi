//
//  ViewControllerHowToPlay.swift
//  IYI
//
//  Created by Daniel Jørgensen & Mikkel Schmøde on 20/11/2018.
//  Copyright © 2018 MD. All rights reserved.
//

import UIKit
import AVFoundation

class ViewControllerHowToPlay: UIViewController {
    
    let back = Bundle.main.path(forResource: "back.mp3", ofType:nil)! // back sound
    var audioPlayer = AVAudioPlayer() // the sound player that plays the sounds
    
    //This is the back button that goes to the last viewController (This will always be the menu view controller)
    @IBAction func HowToPlay(_ sender: Any) {
        // Loads the sound file
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: back))
            audioPlayer.play() // plays the sound file
        } catch {
            print("couldn't load sound file")
        }
        dismiss(animated: true, completion: nil)
    }
}
